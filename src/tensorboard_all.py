from tqdm import tqdm
import torchvision
import torchvision.transforms as transforms

import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
from torch.utils.tensorboard import SummaryWriter

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# transforms
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5,), (0.5,))])

# datasets
trainset = torchvision.datasets.MNIST('./data',
                                      download=True,
                                      train=True,
                                      transform=transform)

testset = torchvision.datasets.MNIST('./data',
                                     download=True,
                                     train=False,
                                     transform=transform)

# dataloaders
trainloader = torch.utils.data.DataLoader(trainset, batch_size=100,
                                          shuffle=True, num_workers=2)


testloader = torch.utils.data.DataLoader(testset, batch_size=100,
                                         shuffle=False, num_workers=2)

# constant for classes
classes = ('0', '1', '2', '3', '4',
           '5', '6', '7', '8', '9')


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 4 * 4, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 4 * 4)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


net = Net()
net.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


# default `log_dir` is "runs" - we'll be more specific here
writer = SummaryWriter('runs/mnist_experiment_1')

# get some random training images
dataiter = iter(testloader)
images, labels = dataiter.next()

# create grid of images
img_grid = torchvision.utils.make_grid(images)


# write to tensorboard
writer.add_image('mnist_images', img_grid)

class_labels = [classes[lab] for lab in labels.numpy()]

for epoch in range(10):  # loop over the dataset multiple times
    running_loss = 0.0
    running_acc = 0.0
    pbar = tqdm(enumerate(trainloader), total=len(trainloader))
    for i, data in pbar:

        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        idx = torch.argmax(outputs, 1)
        running_acc += torch.sum(idx == labels).item()
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        running_loss += loss.item() * labels.size(0)
        pbar.set_description(f"epoch {epoch} iter {i}: train loss {loss.item():.5f}.")
    loss_avg = running_loss / len(trainset)
    acc_avg = running_acc / len(trainset)
    print("epoch {} training loss {:.4} accuracy {:.4}".format(epoch,
                                                               loss_avg,
                                                               acc_avg))


    writer.add_scalar('training/loss',
                      loss_avg,
                      epoch)
    writer.add_scalar('training/acc',
                      acc_avg,
                      epoch)

print('Finished Training')

features = net(images.to(device))
writer.add_embedding(features,
                     metadata=class_labels,
                     label_img=images)

writer.flush()
print()
total = 0
correct = 0
with torch.no_grad():
    for i, data in enumerate(testloader):
        images, labels = data
        images.to(device)
        labels.to(device)
        outputs = net(images.to(device))
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels.to(device)).sum().item()

print('Accuracy of the network on the test images: %d %%' % (
    100 * correct / total))
